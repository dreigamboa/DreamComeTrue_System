package model;

import java.sql.Time;

public class schedBean {
	private String teacher;
	private String student;
	private java.sql.Date schedDate;
	private Time fromTime;
	private Time toTime;
	public String getTeacher() {
		return teacher;
	}
	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	public String getStudent() {
		return student;
	}
	public void setStudent(String student) {
		this.student = student;
	}
	public java.sql.Date getSchedDate() {
		return schedDate;
	}
	public void setSchedDate(java.sql.Date schedDate) {
		this.schedDate = schedDate;
	}
	public Time getFromTime() {
		return fromTime;
	}
	public void setFromTime(Time fromTime) {
		this.fromTime = fromTime;
	}
	public Time getToTime() {
		return toTime;
	}
	public void setToTime(Time toTime) {
		this.toTime = toTime;
	}
	
	
	
	
}
