package model;


public class accountBean {
	private String Username;
	private String Password;
	private String Role;
	private String FirstName;
	private String LastName;
	private String Email;
	private String Address;
	private String Contact;
	private java.math.BigDecimal Salary;
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String getRole() {
		return Role;
	}
	public void setRole(String role) {
		Role = role;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getContact() {
		return Contact;
	}
	public void setContact(String contact) {
		Contact = contact;
	}
	public java.math.BigDecimal getSalary() {
		return Salary;
	}
	public void setSalary(java.math.BigDecimal salary) {
		Salary = salary;
	}
	
	
}
