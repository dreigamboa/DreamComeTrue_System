package utility;

import java.sql.*;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import model.accountBean;
import model.schedBean;

public class SQLOperations implements SQLCommands {
private static Connection connection;
	
	private static Connection getDBConnection() {
		try {
		    InitialContext context = new InitialContext();
		    DataSource dataSource = (DataSource) 
		     context.lookup("java:comp/env/jdbc/DreamComeTrue");
		    
		    if (dataSource != null) {
		    	connection = dataSource.getConnection();
		    }
		} catch (NamingException e) {
		    e.printStackTrace();
		} catch (SQLException e) {
		    e.printStackTrace();
		}
		return connection;
	 }
	
	public static Connection getConnection() {
		return (connection!=null)?connection:getDBConnection();
	}
	
	public static boolean login(accountBean ab, Connection conn){
		try{
			PreparedStatement st=conn.prepareStatement(LOGIN);
			st.setString(1, ab.getUsername());
			st.setString(2, ab.getPassword());
			ResultSet rs=st.executeQuery();
			rs.next();
			ab.setRole(rs.getString("role"));
			ab.setFirstName(rs.getString("firstname"));
			ab.setLastName(rs.getString("lastname"));
			ab.setEmail(rs.getString("email"));
			ab.setAddress(rs.getString("address"));
			ab.setContact(rs.getString("contact"));
			ab.setSalary(rs.getBigDecimal("salary"));
			return true;
		}
		catch(SQLException e){
			System.out.println("Login had an error: "+e.getMessage());
			return false;
		}
	}
	
	public static ResultSet teacherList(Connection conn){
		try{
			Statement stmt=conn.createStatement();
			ResultSet rs=stmt.executeQuery(TEACHERLIST);
			return rs;
		}
		catch(SQLException e){
			System.out.println("Teacherlist had an error: "+e.getMessage());
			return null;
		}
	}
	
	public static ResultSet studentList(Connection conn){
		try{
			Statement stmt=conn.createStatement();
			ResultSet rs=stmt.executeQuery(STUDENTLIST);
			return rs;
		}
		catch(SQLException e){
			System.out.println("Studentlist had an error: "+e.getMessage());
			return null;
		}
	}
	
	public static boolean newSched(schedBean sb, Connection conn){
		try{
			PreparedStatement pstmt=conn.prepareStatement(NEWSCHED);
			pstmt.setString(1, sb.getTeacher());
			pstmt.setString(2, sb.getStudent());
			pstmt.setDate(3, sb.getSchedDate());
			pstmt.setTime(4, sb.getFromTime());
			pstmt.setTime(5, sb.getToTime());
			pstmt.executeUpdate();
			return true;
		}catch(Exception e){
			System.out.println("newSched had an error: "+e.getMessage());
			return false;
		}
	}
}
