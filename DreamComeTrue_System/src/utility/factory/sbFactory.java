package utility.factory;
import java.sql.Time;

import model.schedBean;
public class sbFactory {
	public static schedBean sbFac(String teacher, String student, java.sql.Date schedDate, Time fromTime, Time toTime){
		schedBean sb=new schedBean();
		sb.setTeacher(teacher);
		sb.setStudent(student);
		sb.setSchedDate(schedDate);
		sb.setFromTime(fromTime);
		sb.setToTime(toTime);
		return sb;
	}
}
