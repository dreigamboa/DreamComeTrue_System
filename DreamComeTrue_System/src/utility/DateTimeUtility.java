package utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtility {
	
	@SuppressWarnings("deprecation")
	public static java.sql.Date schedDate(String in){
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("MMMMM dd, yyyy");
			Date date=sdf.parse(in);
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.MONTH,date.getMonth());
			cal.set(Calendar.DAY_OF_MONTH,date.getDate());
			cal.set(Calendar.YEAR,date.getYear()+1900);
			java.sql.Date finale=new java.sql.Date(date.getYear(),date.getMonth(), date.getDate());
			return finale;
		}catch(Exception e){System.out.println("DateTimeUtil had an error: "+e.toString());return null;}
		
	}
	
	public static java.sql.Time TimeFinale(int[] in){
		try{
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, in[0]);
			cal.set(Calendar.MINUTE, in[1]);
			@SuppressWarnings("deprecation")
			java.sql.Time jst=new java.sql.Time(in[0]+in[2], in[1],0);
			return jst;
		}catch(Exception e){System.out.println("TimeFinale had an error: "+e.toString());return null;}
	}

	public static int[] TimeProc(String in){
		try{
			String ina[]=in.split(" ");
			String inb[]=ina[0].split(":");
			int out[]=new int[3];
			int t1=Integer.parseInt(inb[0]);
			int t2=Integer.parseInt(inb[1]);
			if(ina[1].equals("PM")){
				out[2]=12;
			}else{out[2]=0;}
			out[0]=t1;
			out[1]=t2;
			return out;
		}catch(Exception e){System.out.println("TimeProc had an error: "+e.toString());return null;}
	}
}
