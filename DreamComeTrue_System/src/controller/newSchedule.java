package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utility.DateTimeUtility;
import utility.SQLOperations;
import model.schedBean;
import utility.factory.*;

/**
 * Servlet implementation class newSchedule
 */
@WebServlet("/newSchedule")
public class newSchedule extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
       
	private Connection connection;   
	public void init() throws ServletException {
		connection = (Connection) 
				getServletContext().getAttribute("dbConnection");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		try{
			HttpSession session=request.getSession();
			String teacher=request.getParameter("employee");
			String student=request.getParameter("student");
			String in=request.getParameter("notice");if(in==""){throw new Exception();}
			String stime=request.getParameter("starttime");
			String etime=request.getParameter("endtime");if(stime.compareTo(etime)==0){throw new Exception();}
			java.sql.Date schedDate=DateTimeUtility.schedDate(in);if(schedDate.before(new java.sql.Date(Calendar.getInstance().getTimeInMillis()))){throw new Exception();};
			java.sql.Time fromTime=DateTimeUtility.TimeFinale(DateTimeUtility.TimeProc(stime));
			java.sql.Time toTime=DateTimeUtility.TimeFinale(DateTimeUtility.TimeProc(etime));
			schedBean sb=sbFactory.sbFac(teacher, student, schedDate, fromTime, toTime);
			if(SQLOperations.newSched(sb, connection)){
				ResultSet rs=SQLOperations.teacherList(connection);
				ResultSet rs2=SQLOperations.studentList(connection);
				session.setAttribute("teacher", rs);
				session.setAttribute("student", rs2);
				response.setContentType("text/html;charset=UTF-8");
				out.println("<script type=\"text/javascript\">");
				out.println("alert('Schedule has been registered. Thank you.');");
				out.println("location='home.jsp';");
				out.println("</script>");
			}
		}catch(Exception e){
			System.out.println("newSchedServlet had an error: "+e.getMessage());
			HttpSession session=request.getSession();
			ResultSet rs=SQLOperations.teacherList(connection);
			ResultSet rs2=SQLOperations.studentList(connection);
			session.setAttribute("teacher", rs);
			session.setAttribute("student", rs2);
			response.setContentType("text/html;charset=UTF-8");
			out.println("<script type=\"text/javascript\">");
			out.println("alert('Error Occured. Recheck your entry before resubmitting.');");
			out.println("location='home.jsp';");
			out.println("</script>");
		}
	}
}
