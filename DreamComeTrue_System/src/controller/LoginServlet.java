package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.accountBean;
import utility.SQLOperations;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/loginservlet")
public class LoginServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private Connection connection;
		
		public void init() throws ServletException {
			connection = SQLOperations.getConnection();
			
			if (connection != null) {
				getServletContext().setAttribute("dbConnection", connection);
				System.out.println("connection is READY.");
			} else {
				System.err.println("connection is NULL.");
			}
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		accountBean ab=new accountBean();
		String username=request.getParameter("un");
		String password=request.getParameter("pw");
		ab.setUsername(username);
		ab.setPassword(password);
		if(SQLOperations.login(ab, connection)){
			ResultSet rs=SQLOperations.teacherList(connection);
			ResultSet rs2=SQLOperations.studentList(connection);
			HttpSession session=request.getSession();
			session.setAttribute("name", ab.getFirstName()+" "+ab.getLastName());
			session.setAttribute("teacher", rs);
			session.setAttribute("student", rs2);
			if(ab.getRole().compareToIgnoreCase("headteacher")==0){
				getServletContext().getRequestDispatcher("/home.jsp").forward(request, response);
			}
			else{
				getServletContext().getRequestDispatcher("/home2.jsp").forward(request, response);
			}
		}
		else{
			PrintWriter out=response.getWriter();
			response.setContentType("text/html;charset=UTF-8");
			out.println("<script type=\"text/javascript\">");
			out.println("alert('Login failed. Try again.');");
			out.println("location='index.jsp';");
			out.println("</script>");
		}		
	}

}
