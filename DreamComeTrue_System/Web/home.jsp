<!DOCTYPE html>
<html>
	<jsp:useBean id="teacher" type="java.sql.ResultSet" scope="session"/>
	<jsp:useBean id="student" type="java.sql.ResultSet" scope="session"/>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Dream Come True</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="css/jquery-ui.css" />
		<script src="js/jquery.js"></script>
		<script src="js/jquery-ui.js"></script>
		<script src="js/bootstrap.min.js"></script>
		
		
		<script>
			$(function() {
				$("#datepicker").datepicker({
						inline: true,
						dateFormat: 'MM d, yy',
						showOtherMonths: true,
						dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
				});

				$( "#datepicker1" ).datepicker();
			});
			
			
			$(document).on("change", "#datepicker", function () {
				var currentDate = $( "#datepicker" ).datepicker().val();
				$('#notice').val(currentDate);
				$('#notice2').val(currentDate);
			});
		</script>
		
	</head>
  
	<body>
		<div class="main">
			<div class="tabs">
				<div class="container-fluid">
					<div class="row" id="row1">
						Welcome, <em><a href="home.jsp" class="white"><%=session.getAttribute("name") %></a></em>&nbsp&nbsp<a href="Logout" class="white"><span class="glyphicon glyphicon-off"></span></a>
					</div>
					<div class="row" id="row2">
						<h1 style="color: white;">Dream Come True</h1>
					</div>
				</div>
				<ul class="nav nav-tabs" id="navbar">
				  <li class="active"><a href="#home" data-toggle="tab" class="white">Home</a></li>
				  <li><a href="#schedule" data-toggle="tab" class="white">Manage Schedules</a></li>
				  <li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Manage Accounts
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					  <li><a href="#employees" data-toggle="tab">Employees</a></li>
					  <li><a href="#students" data-toggle="tab">Students</a></li>
					</ul>
				  </li>
				  <li><a href="#payroll" data-toggle="tab" class="white">Payroll Computation</a></li>
				</ul>
			</div>
			
			<div class="container" id="c1">
				<div class="tab-content">
					<div id="home" class="tab-pane fade in active">
					  <h3>Welcome to <em>Dream Come True</em> Online Management System</h3>
					  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					
					<!-- Manage Schedules -->
					
					<div id="schedule" class="tab-pane fade">
					  <div style="display: inline-block; vertical-align: top; margin-top: 20px;">
						  <div id="datepicker"></div>
						  <div id="datepicker1" class="hasDatepicker"></div>
						  <br/>
						  <div style="margin-left: 60px;">
								<button class="submitbtn" style="width: 400px; padding: 10px; margin-bottom: 50px;" data-toggle="modal" data-target="#listModal">List of Schedules for this Date</button>
						  </div>
					  </div>
					  <div style="display: inline-block; margin-left: 100px;">
						<h1>Date:</h1>
						<form action="newSchedule" method="post">
						<input type="text" class="notice" id="notice" style="color: black; font-size:20px;background-color: white; border:0px;" name="notice" readonly>
						<br/>
						<br/>
						<label>Employee: </label>
						<select name="employee" id="employee">
							<%while(teacher.next()){ out.print("<option value="+teacher.getString("username")+">"+teacher.getString("firstname")+" "+teacher.getString("lastname")+"</option>");} %>
						</select>
						<br/>
						<label>Student: </label>
						<select name="student" id="student">
							<%while(student.next()){ out.print("<option value="+student.getString("username")+">"+student.getString("firstname")+" "+student.getString("lastname")+"</option>");} %>
						</select>
						<br/>
						<label>From:</label>
						<select name="starttime" id="time">
							<option value="5:00 AM">5:00 AM</option>
							<option value="5:15 AM">5:15 AM</option>
							<option value="5:30 AM">5:30 AM</option>
							<option value="5:45 AM">5:45 AM</option>
							<option value="6:00 AM">6:00 AM</option>
							<option value="6:15 AM">6:15 AM</option>
							<option value="6:30 AM">6:30 AM</option>
							<option value="6:45 AM">6:45 AM</option>
							<option value="7:00 AM">7:00 AM</option>
							<option value="7:15 AM">7:15 AM</option>
							<option value="7:30 AM">7:30 AM</option>
							<option value="7:45 AM">7:45 AM</option>
							<option value="8:00 AM">8:00 AM</option>
							<option value="8:15 AM">8:15 AM</option>
							<option value="8:30 AM">8:30 AM</option>
							<option value="8:45 AM">8:45 AM</option>
							<option value="9:00 AM">9:00 AM</option>
							<option value="9:15 AM">9:15 AM</option>
							<option value="9:30 AM">9:30 AM</option>
							<option value="9:45 AM">9:45 AM</option>
							<option value="10:00 AM">10:00 AM</option>
							<option value="10:15 AM">10:15 AM</option>
							<option value="10:30 AM">10:30 AM</option>
							<option value="10:45 AM">10:45 AM</option>
							<option value="11:00 AM">11:00 AM</option>
							<option value="11:15 AM">11:15 AM</option>
							<option value="11:30 AM">11:30 AM</option>
							<option value="11:45 AM">11:45 AM</option>
							<option value="12:00 PM">12:00 PM</option>
							<option value="12:15 PM">12:15 PM</option>
							<option value="12:30 PM">12:30 PM</option>
							<option value="12:45 PM">12:45 PM</option>
							<option value="1:00 PM">1:00 PM</option>
							<option value="1:15 PM">1:15 PM</option>
							<option value="1:30 PM">1:30 PM</option>
							<option value="1:45 PM">1:45 PM</option>
							<option value="2:00 PM">2:00 PM</option>
							<option value="2:15 PM">2:15 PM</option>
							<option value="2:30 PM">2:30 PM</option>
							<option value="2:45 PM">2:45 PM</option>
							<option value="3:00 PM">3:00 PM</option>
							<option value="3:15 PM">3:15 PM</option>
							<option value="3:30 PM">3:30 PM</option>
							<option value="3:45 PM">3:45 PM</option>
							<option value="4:00 PM">4:00 PM</option>
							<option value="4:15 PM">4:15 PM</option>
							<option value="4:30 PM">4:30 PM</option>
							<option value="4:45 PM">4:45 PM</option>
							<option value="5:00 PM">5:00 PM</option>
							<option value="5:15 PM">5:15 PM</option>
							<option value="5:30 PM">5:30 PM</option>
							<option value="5:45 PM">5:45 PM</option>
							<option value="6:00 PM">6:00 PM</option>
							<option value="6:15 PM">6:15 PM</option>
							<option value="6:30 PM">6:30 PM</option>
							<option value="6:45 PM">6:45 PM</option>
							<option value="7:00 PM">7:00 PM</option>
							<option value="7:15 PM">7:15 PM</option>
							<option value="7:30 PM">7:30 PM</option>
							<option value="7:45 PM">7:45 PM</option>
							<option value="8:00 PM">8:00 PM</option>
							<option value="8:15 PM">8:15 PM</option>
							<option value="8:30 PM">8:30 PM</option>
							<option value="8:45 PM">8:45 PM</option>
							<option value="9:00 PM">9:00 PM</option>
							<option value="9:15 PM">9:15 PM</option>
							<option value="9:30 PM">9:30 PM</option>
							<option value="9:45 PM">9:45 PM</option>
							<option value="10:00 PM">10:00 PM</option>
							<option value="10:15 PM">10:15 PM</option>
							<option value="10:30 PM">10:30 PM</option>
							<option value="10:45 PM">10:45 PM</option>
							<option value="11:00 PM">11:00 PM</option>
							<option value="11:15 PM">11:15 PM</option>
							<option value="11:30 PM">11:30 PM</option>
							<option value="11:45 PM">11:45 PM</option>
						</select>
						<br/>
						<label>To:</label>
						<select name="endtime" id="time">
							<option value="5:00 AM">5:00 AM</option>
							<option value="5:15 AM">5:15 AM</option>
							<option value="5:30 AM">5:30 AM</option>
							<option value="5:45 AM">5:45 AM</option>
							<option value="6:00 AM">6:00 AM</option>
							<option value="6:15 AM">6:15 AM</option>
							<option value="6:30 AM">6:30 AM</option>
							<option value="6:45 AM">6:45 AM</option>
							<option value="7:00 AM">7:00 AM</option>
							<option value="7:15 AM">7:15 AM</option>
							<option value="7:30 AM">7:30 AM</option>
							<option value="7:45 AM">7:45 AM</option>
							<option value="8:00 AM">8:00 AM</option>
							<option value="8:15 AM">8:15 AM</option>
							<option value="8:30 AM">8:30 AM</option>
							<option value="8:45 AM">8:45 AM</option>
							<option value="9:00 AM">9:00 AM</option>
							<option value="9:15 AM">9:15 AM</option>
							<option value="9:30 AM">9:30 AM</option>
							<option value="9:45 AM">9:45 AM</option>
							<option value="10:00 AM">10:00 AM</option>
							<option value="10:15 AM">10:15 AM</option>
							<option value="10:30 AM">10:30 AM</option>
							<option value="10:45 AM">10:45 AM</option>
							<option value="11:00 AM">11:00 AM</option>
							<option value="11:15 AM">11:15 AM</option>
							<option value="11:30 AM">11:30 AM</option>
							<option value="11:45 AM">11:45 AM</option>
							<option value="12:00 PM">12:00 PM</option>
							<option value="12:15 PM">12:15 PM</option>
							<option value="12:30 PM">12:30 PM</option>
							<option value="12:45 PM">12:45 PM</option>
							<option value="1:00 PM">1:00 PM</option>
							<option value="1:15 PM">1:15 PM</option>
							<option value="1:30 PM">1:30 PM</option>
							<option value="1:45 PM">1:45 PM</option>
							<option value="2:00 PM">2:00 PM</option>
							<option value="2:15 PM">2:15 PM</option>
							<option value="2:30 PM">2:30 PM</option>
							<option value="2:45 PM">2:45 PM</option>
							<option value="3:00 PM">3:00 PM</option>
							<option value="3:15 PM">3:15 PM</option>
							<option value="3:30 PM">3:30 PM</option>
							<option value="3:45 PM">3:45 PM</option>
							<option value="4:00 PM">4:00 PM</option>
							<option value="4:15 PM">4:15 PM</option>
							<option value="4:30 PM">4:30 PM</option>
							<option value="4:45 PM">4:45 PM</option>
							<option value="5:00 PM">5:00 PM</option>
							<option value="5:15 PM">5:15 PM</option>
							<option value="5:30 PM">5:30 PM</option>
							<option value="5:45 PM">5:45 PM</option>
							<option value="6:00 PM">6:00 PM</option>
							<option value="6:15 PM">6:15 PM</option>
							<option value="6:30 PM">6:30 PM</option>
							<option value="6:45 PM">6:45 PM</option>
							<option value="7:00 PM">7:00 PM</option>
							<option value="7:15 PM">7:15 PM</option>
							<option value="7:30 PM">7:30 PM</option>
							<option value="7:45 PM">7:45 PM</option>
							<option value="8:00 PM">8:00 PM</option>
							<option value="8:15 PM">8:15 PM</option>
							<option value="8:30 PM">8:30 PM</option>
							<option value="8:45 PM">8:45 PM</option>
							<option value="9:00 PM">9:00 PM</option>
							<option value="9:15 PM">9:15 PM</option>
							<option value="9:30 PM">9:30 PM</option>
							<option value="9:45 PM">9:45 PM</option>
							<option value="10:00 PM">10:00 PM</option>
							<option value="10:15 PM">10:15 PM</option>
							<option value="10:30 PM">10:30 PM</option>
							<option value="10:45 PM">10:45 PM</option>
							<option value="11:00 PM">11:00 PM</option>
							<option value="11:15 PM">11:15 PM</option>
							<option value="11:30 PM">11:30 PM</option>
							<option value="11:45 PM">11:45 PM</option>
						</select>
						<br/>
						<br/>
						<button type="submit" class="submitbtn" style="padding: 10px; width: 100px;"><span class="glyphicon glyphicon-floppy-save" style="font-size: 20px;"><font style="font-family: Bebas Neue Regular; margin-left: 5px;">Save</font></span></button>
						</form>
					  </div>
					  
					</div>
					
					<!-- Manage Accounts -->
					<!-- Employees -->
					<div id="employees" class="tab-pane fade">
						<div style="display: inline-block;">
							<a href="#add" data-toggle="tab"><button class="employeebtn">Add Employee</button></a>
							<br/>
							<br/>
							<a href="#changePassword" data-toggle="tab"><button class="employeebtn">Change Employee Password</button></a>
							<br/>
							<br/>
							<a href="#listEmployees" data-toggle="tab"><button class="employeebtn">List Employees</button></a>
						</div>
						
						<div class="tab-content" style="display: inline-block; vertical-align: top; margin-top: -20px; margin-left: 100px;">
							<div id="add" class="tab-pane fade">
								<form onsubmit="alert('Employee has been added');">
									<h1>Add Employee</h1>
									<br/>
									<label>First Name:</label><input type="text" name="firstName" id="firstName" class="employeeinput">
									<br/>
									<label>Last Name:</label><input type="text" name="lastName" id="lastName" class="employeeinput">
									<br/>
									<label>Username:</label><input type="text" name="userName" id="username" class="employeeinput">
									<br/>
									<label>Password:</label><input type="text" name="password" id="password" class="employeeinput">
									<br/>
									<label>Confirm Password:</label><input type="text" name="password" id="password" class="employeeinput">
									<br/>
									<br/>
									<button type="submit" class="submitbtn" style="padding: 10px; width: 150px;"><span class="glyphicon glyphicon-floppy-save" style="font-size: 20px;"><font style="font-family: Bebas Neue Regular; margin-left: 5px;">Add Employee</font></span></button>
								</form>
							</div>
							<div id="changePassword" class="tab-pane fade">
								<form onsubmit="alert('Succesfully changed password')">
								<h1>Change Employee Password</h1>
								<br/>
								<label>Employee: </label>
								<select name="employee" id="employee">
									<option value="Employee1">Employee 1</option>
									<option value="Employee1">Employee 2</option>
									<option value="Employee1">Employee 3</option>
									<option value="Employee1">Employee 4</option>
								</select>
								<br/>
								<label>New Password:</label><input type="text" name="password" id="password" class="employeeinput">
								<br/>
								<label>Confirm New Password:</label><input type="text" name="passwordconfirm" id="passwordconfirm" class="employeeinput">
								<br/>
								<br/>
								<button type="submit" class="submitbtn" style="padding: 10px; width: 180px;"><span class="glyphicon glyphicon-floppy-save" style="font-size: 20px;"><font style="font-family: Bebas Neue Regular; margin-left: 5px;">Save New Password</font></span></button>
								</form>
							</div>
							<div id="listEmployees" class="tab-pane fade">
								<h1>List of all Employees</h1>
								<table class="table table-hover">
									<thead>
										<tr>
											<th>First Name</th>
											<th>Last Name</th>
											<th>Username</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>First Name</td>
											<td>Last Name</td>
											<td>Username</td>
											<td><span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#editModal"></span></td></a>
											<td><span class="glyphicon glyphicon-remove"></span></td>
										</tr>
										<tr>
											<td>First Name</td>
											<td>Last Name</td>
											<td>Username</td>
											<td><span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#editModal"></span></td></a>
											<td><span class="glyphicon glyphicon-remove"></span></td>
										</tr>
										<tr>
											<td>First Name</td>
											<td>Last Name</td>
											<td>Username</td>
											<td><span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#editModal"></span></td></a>
											<td><span class="glyphicon glyphicon-remove"></span></td>
										</tr>
										<tr>
											<td>First Name</td>
											<td>Last Name</td>
											<td>Username</td>
											<td><span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#editModal"></span></td></a>
											<td><span class="glyphicon glyphicon-remove"></span></td>
										</tr>
										<tr>
											<td>First Name</td>
											<td>Last Name</td>
											<td>Username</td>
											<td><span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#editModal"></span></td></a>
											<td><span class="glyphicon glyphicon-remove"></span></td>
										</tr>
										<tr>
											<td>First Name</td>
											<td>Last Name</td>
											<td>Username</td>
											<td><span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#editModal"></span></td></a>
											<td><span class="glyphicon glyphicon-remove"></span></td>
										</tr>
										<tr>
											<td>First Name</td>
											<td>Last Name</td>
											<td>Username</td>
											<td><span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#editModal"></span></td></a>
											<td><span class="glyphicon glyphicon-remove"></span></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					
					<!-- Students -->
					
					<div id="students" class="tab-pane fade">
						<div style="display: inline-block;">
							<a href="#addStudent" data-toggle="tab"><button class="employeebtn">Add Student</button></a>
							<br/>
							<br/>
							<a href="#listStudents" data-toggle="tab"><button class="employeebtn">List Students</button></a>
						</div>
						<div class="tab-content" style="display: inline-block; vertical-align: top; margin-top: -20px; margin-left: 100px;">
							<div id="addStudent" class="tab-pane fade">
								<form onsubmit="alert('Student has been added');">
									<h1>Add Student</h1>
									<br/>
									<label>First Name:</label><input type="text" name="studentFirstName" id="studentFirstName" class="employeeinput">
									<br/>
									<label>Last Name:</label><input type="text" name="studentLastName" id="studentLastName" class="employeeinput">
									<br/>
									<label>Skype Username:</label><input type="text" name="skypeUserName" id="skypeUsername" class="employeeinput">
									<br/>
									<label>Contact Number:</label><input type="text" name="contact" id="contact" class="employeeinput">
									<br/>
									<br/>
									<button type="submit" class="submitbtn" style="padding: 10px; width: 150px;"><span class="glyphicon glyphicon-floppy-save" style="font-size: 20px;"><font style="font-family: Bebas Neue Regular; margin-left: 5px;">Add Student</font></span></button>
								</form>
							</div>
							<div id="listStudents" class="tab-pane fade">
								<table class="table table-hover">
									<h1>List of all Students</h1>
									<thead>
										<tr>
											<th>First Name</th>
											<th>Last Name</th>
											<th>Skype Username</th>
											<th>Contact Number</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>First Name</td>
											<td>Last Name</td>
											<td>Skype Username</td>
											<td>Contact Number</td>
											<td><span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#editStudentModal"></span></td></a>
											<td><span class="glyphicon glyphicon-remove"></span></td>
										</tr>
										<tr>
											<td>First Name</td>
											<td>Last Name</td>
											<td>Skype Username</td>
											<td>Contact Number</td>
											<td><span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#editStudentModal"></span></td></a>
											<td><span class="glyphicon glyphicon-remove"></span></td>
										</tr>
										<tr>
											<td>First Name</td>
											<td>Last Name</td>
											<td>Skype Username</td>
											<td>Contact Number</td>
											<td><span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#editStudentModal"></span></td></a>
											<td><span class="glyphicon glyphicon-remove"></span></td>
										</tr>
										<tr>
											<td>First Name</td>
											<td>Last Name</td>
											<td>Skype Username</td>
											<td>Contact Number</td>
											<td><span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#editStudentModal"></span></td></a>
											<td><span class="glyphicon glyphicon-remove"></span></td>
										</tr>
										<tr>
											<td>First Name</td>
											<td>Last Name</td>
											<td>Skype Username</td>
											<td>Contact Number</td>
											<td><span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#editStudentModal"></span></td></a>
											<td><span class="glyphicon glyphicon-remove"></span></td>
										</tr>
										<tr>
											<td>First Name</td>
											<td>Last Name</td>
											<td>Skype Username</td>
											<td>Contact Number</td>
											<td><span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#editStudentModal"></span></td></a>
											<td><span class="glyphicon glyphicon-remove"></span></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					
					<!-- Payroll -->
					<div id="payroll" class="tab-pane fade">
						<form onsubmit="alert('Succesfully Computed Payroll')">
							<h1>Payroll Computation</h1>
							<br/>
							<label>Employee: </label>
							<select name="employee" id="employee">
								<option value="Employee1">Employee 1</option>
								<option value="Employee1">Employee 2</option>
								<option value="Employee1">Employee 3</option>
								<option value="Employee1">Employee 4</option>
							</select>
							<br/>
							<label>PayRate: </label><input type="text" name="payRate" id="payRate" class="employeeinput"> php
							<br/>
							<label>Total Hours Worked:</label><input type="text" name="hoursWorked" id="hoursWorked" class="employeeinput" value="12" style="width: 50px;" disabled> hours
							<br/>
							<br/>
							<button type="submit" class="submitbtn" style="padding: 10px; width: 180px;"><span class="glyphicon glyphicon-stats" style="font-size: 20px;"><font style="font-family: Bebas Neue Regular; margin-left: 5px;">Compute Gross Pay</font></span></button>
							<button type="button" class="submitbtn" style="padding: 10px; width: 180px;"><span class="glyphicon glyphicon-stats" style="font-size: 20px;"><font style="font-family: Bebas Neue Regular; margin-left: 5px;" data-toggle="modal" data-target="#listGpayModal">View Gross Pay List</font></span></button>
						</form>
					</div>
					
					<div id="menu3" class="tab-pane fade">
					  <h3>Menu 3</h3>
					  <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
					</div>
				  </div>
			  </div>
		</div>	
		
		<!-- List Employee Modal -->
		<div id="listModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header" style="text-align: center;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">List of all schedules for <input type="text" id="notice2" style="color: black; font-size:25px;background-color: white; border:0px;" value="" readonly></h3>
			  </div>
			  <div class="modal-body" style="text-align: center;">				
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Employee</th>
							<th>Student</th>
							<th>Time Start</th>
							<th>Time End</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>employee1</td>
							<td>student1</td>
							<td>7:00 AM</td>
							<td>8:00 AM</td>
							<td>Attended</td>
						</tr>
						<tr>
							<td>employee1</td>
							<td>student1</td>
							<td>7:00 AM</td>
							<td>8:00 AM</td>
							<td>Not Attended</td>
						</tr>
						<tr>
							<td>employee1</td>
							<td>student1</td>
							<td>7:00 AM</td>
							<td>8:00 AM</td>
							<td>Not Attended</td>
						</tr>
						<tr>
							<td>employee1</td>
							<td>student1</td>
							<td>7:00 AM</td>
							<td>8:00 AM</td>
							<td>Attended</td>
						</tr>
					</tbody>
				</table>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>
		
		
		<!-- List Employee Modal -->
		<div id="listGpayModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header" style="text-align: center;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h1 class="modal-title">Gross Pay List</h1>
			  </div>
			  <div class="modal-body" style="text-align: center;">				
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Employee</th>
							<th>Hours Worked</th>
							<th>Pay Rate</th>
							<th>Gross Pay</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>employee1</td>
							<td>10 hours</td>
							<td>2,000 php</td>
							<td>20,000 php</td>
						</tr>
						<tr>
							<td>employee2</td>
							<td>10 hours</td>
							<td>2,000 php</td>
							<td>20,000 php</td>
						</tr>
						<tr>
							<td>employee3</td>
							<td>10 hours</td>
							<td>2,000 php</td>
							<td>20,000 php</td>
						</tr>
						<tr>
							<td>employee4</td>
							<td>10 hours</td>
							<td>2,000 php</td>
							<td>20,000 php</td>
						</tr>
						<tr>
							<td>employee4</td>
							<td>10 hours</td>
							<td>2,000 php</td>
							<td>20,000 php</td>
						</tr>
					</tbody>
				</table>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>
		
		<!-- Edit Employee Modal -->
		<div id="editModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header" style="text-align: center;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h1 class="modal-title">Edit Employee</h1>
			  </div>
			  <div class="modal-body" style="text-align: center;">				
				<form onsubmit="alert('Succesfully updated employee')">
					<label>First Name:</label><input type="text" name="firstName" id="firstName" class="employeeinput">
					<br/>
					<label>Last Name:</label><input type="text" name="lastName" id="lastName" class="employeeinput">
					<br/>
					<label>Username:</label><input type="text" name="userName" id="username" class="employeeinput">
					<br/>
					<br/>
					<button type="submit" class="submitbtn" style="padding: 10px; width: 150px;"><span class="glyphicon glyphicon-floppy-save" style="font-size: 20px;"><font style="font-family: Bebas Neue Regular; margin-left: 5px;">Update</font></span></button>
				</form>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>
		
		<!-- Edit Student Modal -->
		<div id="editStudentModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header" style="text-align: center;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h1 class="modal-title">Edit Student</h1>
			  </div>
			  <div class="modal-body" style="text-align: center;">				
				<form onsubmit="alert('Succesfully updated student')">
					<label>First Name:</label><input type="text" name="firstName" id="firstName" class="employeeinput">
					<br/>
					<label>Last Name:</label><input type="text" name="lastName" id="lastName" class="employeeinput">
					<br/>
					<label>Skype Username:</label><input type="text" name="userName" id="username" class="employeeinput">
					<br/>
					<label>Contact Number:</label><input type="text" name="userName" id="username" class="employeeinput">
					<br/>
					<br/>
					<button type="submit" class="submitbtn" style="padding: 10px; width: 150px;"><span class="glyphicon glyphicon-floppy-save" style="font-size: 20px;"><font style="font-family: Bebas Neue Regular; margin-left: 5px;">Update</font></span></button>
				</form>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>
		
	</body>
</html>