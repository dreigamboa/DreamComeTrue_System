<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dream Come True</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	
	<script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <script>
		function validateLogin() {
			var unBox = document.querySelector("#un");
			var pwBox = document.querySelector("#pw");
			var noticeboc = document.querySelector("#notice");
			var a = document.getElementById("notice").value;
			var b = document.getElementById("un").value;
			var c = document.getElementById("pw").value;
					
			var count = 0;
						
			// U S E R N A M E
			if (b==null || b== "") {
				document.getElementById("notice").value = "Enter username.";
				return false;
			}
					
					
			// P A S S W O R D 
			if (c==null || c== "") {
				document.getElementById("notice").value = "Enter password.";
				return false;
			}
						
	//		alert("Success");
			return true;
				
		}
	</script>
  </head>
	<body>
		<div class="main">
			<div class="tabs" style="padding-bottom: 10px;">
				<div class="container-fluid">
					<div class="row" id="row2" style="margin-top: 50px;">
						<h1 style="color: white;">Dream Come True</h1>
					</div>
					<div class="row" style="color:white; margin-top: 33px; margin-right: 30px; float: right;">
						<a href="#" class="white"><h4>Help <span class="glyphicon glyphicon-question-sign"></span></h4></a>
					</div>
				</div>
			</div>
			
			<div class="container">
				<div class="row">
					<div class="col-lg-3"></div>
					<div class="col-lg-6" style="text-align: center; margin-top: 100px;">
						<div class="row">
						<input type="text" class="notice" id="notice" style="width:100%; color: red; background-color: white; border:0px; text-align:center; " value="" disabled>
						</div>
						<br/>
						<form action="loginservlet" method="post" onsubmit="return validateLogin()">
						<span class="glyphicon glyphicon-user" style="color: #336699; font-size: 30px; vertical-align: middle;"></span>&nbsp&nbsp&nbsp&nbsp <input type="text" id="un" class="inputlogin" placeholder="Username" name="un">
						<br>
						<br>
						<span class="glyphicon glyphicon-lock" style="color: #336699; font-size: 30px; vertical-align: middle;"></span>&nbsp&nbsp&nbsp&nbsp <input type="password" id="pw" class="inputlogin" placeholder="Password" name="pw">
						<br>
						<br>
						<p style="text-align: right; margin-right: 20px;">
							<a style="color: blue;">Forgot your password?</a>&nbsp&nbsp&nbsp
							<input type="submit" value="Login" class="submitbtn" style="padding: 10px; width: 100px;"/>
						</p>
						
						
						</form>
					</div>
					<div class="col-lg-3"></div>
				</div>
			</div>
			
		</div>	
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
			<script src="js/bootstrap.min.js"></script>
	</body>
</html>
	