<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Dream Come True</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="css/jquery-ui.css" />
		<script src="js/jquery.js"></script>
		<script src="js/jquery-ui.js"></script>
		<script src="js/bootstrap.min.js"></script>
		
		
		<script>
			$(function() {
				$("#datepicker").datepicker({
						inline: true,
						dateFormat: 'MM d, yy',
						showOtherMonths: true,
						dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
				});

				$( "#datepicker1" ).datepicker();
			});
			
			
			$(document).on("change", "#datepicker", function () {
				var currentDate = $( "#datepicker" ).datepicker().val();
				$('#notice').val(currentDate);
				$('#notice2').val(currentDate);
			});
		</script>
		
	</head>
  
	<body>
		<div class="main">
			<div class="tabs">
				<div class="container-fluid">
					<div class="row" id="row1">
						Welcome, <em><a href="home.html" class="white"><%=session.getAttribute("name") %></a></em>&nbsp&nbsp<a href="Logout" class="white"><span class="glyphicon glyphicon-off"></span></a>
					</div>
					<div class="row" id="row2">
						<h1 style="color: white;">Dream Come True</h1>
					</div>
				</div>
				<ul class="nav nav-tabs" id="navbar">
				  <li class="active"><a href="#home" data-toggle="tab" class="white">Home</a></li>
				  <li><a href="#schedule" data-toggle="tab" class="white">Check Schedule</a></li>
				</ul>
			</div>
			
			<div class="container" id="c1">
				<div class="tab-content">
					<div id="home" class="tab-pane fade in active">
					  <h3>Welcome to <em>Dream Come True</em> Online Management System</h3>
					  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					  <hr>
					  <form onsubmit="alert('Your attendance has been confirmed')">
						<h3>It's 10:30 AM, you have a scheduled class</h3>
						<input type="submit" class="submitbtn" style="width: 400px; padding: 10px; margin-bottom: 50px; font-family: Bebas Neue Regular; font-size: 30px;" value="Confirm Attendance">
					  </form>
					</div>
					
					<!-- Manage Schedules -->
					
					<div id="schedule" class="tab-pane fade">
					  <div style="display: inline-block; vertical-align: top; margin-top: 20px; margin-left: -60px;">
						  <div id="datepicker"></div>
						  <div id="datepicker1" class="hasDatepicker"></div>
					  </div>
					  <div style="display: inline-block; margin-left: 20px;">
						<h1>Date:</h1>
							<input type="text" class="notice" id="notice" style="color: black; font-size:20px;background-color: white; border:0px;" value="" readonly>
							<br/>
							<table class="table table-hover">
								<thead>
									<tr>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Skype Username</th>
										<th>Contact Number</th>
										<th>Time Start</th>
										<th>Time End</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Hyun Jung</td>
										<td>Kim</td>
										<td>kimhyunjung</td>
										<td>0936*******</td>
										<td>7:00 AM</td>
										<td>8:00 AM</td>
										<td>Attended<td>
									</tr>
									<tr>
										<td>Hyun Jung</td>
										<td>Kim</td>
										<td>kimhyunjung</td>
										<td>0936*******</td>
										<td>12:00 PM</td>
										<td>1:00 PM</td>
										<td>Attended<td>
									</tr>
									<tr>
										<td>Hyun Jung</td>
										<td>Kim</td>
										<td>kimhyunjung</td>
										<td>0936*******</td>
										<td>7:00 PM</td>
										<td>8:00 PM</td>
										<td>Attended<td>
									</tr>
									<tr>
										<td>Min Ho</td>
										<td>Lee</td>
										<td>leeminho</td>
										<td>0936*******</td>
										<td>10:00 PM</td>
										<td>11:00 PM</td>
										<td>Absent<td>
									</tr>
									<tr>
										<td>Min Ho</td>
										<td>Lee</td>
										<td>leeminho</td>
										<td>0936*******</td>
										<td>1:30 AM</td>
										<td>2:30 AM</td>
										<td>Pending<td>
									</tr>									
								</tbody>
							</table>
					  </div>
					</div> 
	</body>
</html>