CREATE DATABASE DreamComeTrue;

Use DreamComeTrue;

DROP TABLE TEACHERACCOUNTS;

CREATE TABLE TEACHERACCOUNTS(
Username varchar(255) primary key not null,
Password varchar(32) not null,
Role varchar(30) not null,
FirstName varchar(255) not null,
LastName varchar(255) not null,
Email varchar(255) not null,
Address varchar(255) not null,
Contact varchar(11) not null,
Salary numeric(15,2) default 0 not null
);

SELECT * FROM TEACHERACCOUNTS;

INSERT INTO TEACHERACCOUNTS values('admin', 'admin', 'headteacher', 'admin', 'admin', 'admin@admin.com', 'admin address', '09176969696', 0),
('TEACHER1', 'TEACHER1', 'teacher', 'TEACHER1', 'TEACHER1', 'TEACHER1@admin.com', 'TEACHER1 address', '09176969697', 0),
('TEACHER2', 'TEACHER2', 'teacher', 'TEACHER2', 'TEACHER2', 'TEACHER2@admin.com', 'TEACHER2 address', '09176969698', 0);

SELECT * FROM TEACHERACCOUNTS WHERE USERNAME='admin' AND PASSWORD='admin';

SELECT * FROM TEACHERACCOUNTS WHERE ROLE='teacher';

DROP TABLE SCHEDULE;

CREATE TABLE SCHEDULE(
ReferenceNo int auto_increment primary key,
Teacher varchar(255) not null,
Student varchar(255) not null,
SchedDate Date not null,
FromTime Time not null,
ToTime Time not null,
Flag boolean default false);

ALTER TABLE SCHEDULE AUTO_INCREMENT = 1000;

SELECT * FROM SCHEDULE;


CREATE TABLE STUDENTACCOUNTS(
Username varchar(255) primary key not null,
FirstName varchar(255) not null,
LastName varchar(255) not null,
Email varchar(255) not null,
Address varchar(255) not null,
Contact varchar(11) not null
);

INSERT INTO STUDENTACCOUNTS VALUE('Jingjong', 'Jingjong', 'Kurobugak', 'email@kurobugak.com', 'address', '09111111111');

SELECT * FROM STUDENTACCOUNTS;
